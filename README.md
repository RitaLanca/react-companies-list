### Simple React Project 

This Project is a Simple ReactJS Project which demonstrates the following:
    1. Creating Components in React with react hooks
    2. Making HTTP calls
    3. Communicating between parent and child component
    4. Using Basic Routing in React
    5. Using css modules 

#### Prerequisites
1. Install [node.js](https://nodejs.org/en/) which includes [npm](https://npmjs.com)
2. Install create-react-app npm package globally. This will help to easily run the project and also build the source files easily. Use the following command to install create-react-app `npm install -g create-react-app`

## Running

1. Install all the npm packages. Go into the project folder and type the following command to install all npm packages `npm install`

2. In order to run the application Type the following command `npm run dev`

*The api will running in http://localhost:3002.
*The website will running in http://localhost:3000. 


**Note in case of error:**

>Error: 

>json-server : File AppData\Roaming\npm\json-server.ps1 cannot be loaded because running scripts is disabled on this system. For more information, see about_Execution_Policies at https:/go.microsoft.com/fwlink/?LinkID=135170.


1. run windows powershell as admin 
2. run `Set-ExecutionPolicy RemoteSigned -Scope CurrentUser`






