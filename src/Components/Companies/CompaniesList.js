import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'
import Loading from '../Generic/Loading'

import styles from './CompaniesList.module.css'

const CompaniesList = () => {
    const [loading, setLoading] = useState(false);
    const [companies, setCompanies] = useState([]);
    const [error, setError] = useState(null);

    const fetchCompanies = async () => {
        setLoading(true);
        fetch('http://localhost:3002/companies')
            .then(resp => resp.json())
            .then(data => {
                setLoading(false);
                setCompanies(data);
            },
                (error) => {
                    setLoading(false);
                    setError(error);

                }
            );
    }

    useEffect(() => {
        fetchCompanies();
    }, [])

    if (loading) {
        return <Loading />
    }

    if (companies.length === 0) {
        return (
            <main>
                <h2>No Companies</h2>
            </main>
        )
    }
    else {
        return (
            <div className={styles.Container}>
                <h2 className={styles.Title}>Companies</h2>
                <table className={styles.Table}>
                    <thead className={styles.Thead}>
                        <tr>
                            <th>Companies</th>
                            <th>Vatin</th>
                        </tr>
                    </thead>
                    <tbody >
                        {companies.map(company => {
                            return (<tr className={styles.TableRow} key={company.id}>
                                <td>
                                    <Link to={{
                                        pathname: `/companies/${company.id}`,
                                        state: { company: company.name }
                                    }}>{company.name}</Link>
                                </td>
                                <td>{company.vatin}</td>
                            </tr>)
                        })}
                    </tbody>
                </table>
            </div>
        )
    }
}
export default CompaniesList;