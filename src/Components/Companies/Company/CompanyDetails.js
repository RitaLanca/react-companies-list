import React, { useState, useEffect } from 'react'
import { FaChevronLeft } from 'react-icons/fa';
import {
    BrowserRouter as Router,
    useHistory,
    useLocation,
    Link,
    useParams,
} from "react-router-dom";
import Button from '../../Generic/Button/Button'
import styles from './CompanyDetails.module.css'
import Loading from '../../Generic/Loading'

const CompanyDetails = () => {
    const [loading, setLoading] = useState(false);
    const [company, setCompany] = useState(null);
    const [phoneNumbers, setPhoneNumbers] = useState([]);
    const [error, setError] = useState(null);


    let history = useHistory();
    const location = useLocation()
    const companyName = location.state?.company;
    let { id } = useParams();

    const fetchDetails = (id) => {
        fetch('http://localhost:3002/phone_numbers')
            .then(resp => resp.json())
            .then(data => {
                setLoading(false);
                const numbers = data.filter(company => company.company_id == id);
                setPhoneNumbers(numbers);
            },
                (error) => {
                    setLoading(false);
                    setError(error);
                }
            );
    }

    useEffect(() => {
        fetchDetails(id);
    }, []);

    if (loading) {
        return <Loading />
    }
    return (
        <div className={styles.Container}>
            <h2 className={styles.Title}>{companyName}</h2>
            <Button goBack={() => history.goBack()} label="Go Back" icon={<FaChevronLeft />} />
            {
                phoneNumbers.length > 0
                    ?
                    (
                        <table className={styles.Table}>
                            <thead className={styles.Thead}>
                                <tr>
                                    <th>Number</th>
                                    <th>Type</th>
                                </tr>
                            </thead>
                            <tbody>
                                {phoneNumbers.map(phone => {
                                    return (
                                        <tr className={styles.TableRow} key={phone.id}>
                                            <td><Link to={`/numbers/${phone.id}`}>{phone.id}</Link></td>
                                            <td>{phone.type}</td>
                                        </tr>
                                    );
                                })
                                }
                            </tbody>
                        </table>
                    )
                    :
                    <p className={styles.Info}>No data available</p>
            }
        </div>
    )
}

export default CompanyDetails;

