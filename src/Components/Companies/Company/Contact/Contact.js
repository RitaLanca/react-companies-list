
import React, { useState, useEffect } from 'react'
import { FaPhoneAlt, FaMobileAlt, FaChevronLeft } from 'react-icons/fa';

import {
    BrowserRouter as Router,
    useHistory,
    Link,
    useParams,
} from "react-router-dom";
import styles from './Contact.module.css'
import Button from '../../../Generic/Button/Button'
import Loading from '../../../Generic/Loading'

const Contact = () => {
    const [contact, setContact] = useState([]);
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(false);

    let history = useHistory();
    let { number } = useParams();

    const fetchContactDetail = (number) => {
        fetch('http://localhost:3002/phone_numbers')
            .then(resp => resp.json())
            .then(data => {
                setLoading(false);
                const numbers = data.filter(phone => phone.id == number);
                setContact(numbers);
            },
                (error) => {
                    setLoading(false);
                    setError(error);
                }
            );
    }

    useEffect(() => {
        fetchContactDetail(number)

    }, [])

    if (loading) {
        return <Loading />
    }
    return (
        <div className={styles.Container}>
            <Button goBack={() => history.goBack()} label="Go Back" icon={<FaChevronLeft />} />
            {contact.length > 0
                ?
                (<table className={styles.Table}>
                    { contact.map(c => {
                        return (
                            <tr key={c.id} className={styles.TableRow}>
                                <td>{c.type !== 'mobile' ? <FaPhoneAlt /> : <FaMobileAlt />}</td>
                                <td>{c.type}</td>
                                <td>{c.id}</td>
                            </tr>
                        )
                    })
                    }
                </table>)
                :
                <p className={styles.Info}>No data available</p>
            }
        </div>
    )

}

export default Contact