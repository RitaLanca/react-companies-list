import React from 'react'
import styles from './Button.module.css'

const Button = ({ goBack, label, icon }) => {
    return (
        <button className={styles.Btn} onClick={goBack} >
            {icon}
            {label}
        </button>
    )

}

export default Button