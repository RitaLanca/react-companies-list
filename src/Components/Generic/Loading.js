import React from 'react'
import { FaSpinner } from 'react-icons/fa'
import styles from './Loading.module.css'
const Loading = () => {
    return <div className={styles.Spinner}>{<FaSpinner />} </div>

}

export default Loading