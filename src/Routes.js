import React from 'react'

import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import CompaniesList from './Components/Companies/CompaniesList';
import Company from './Components/Companies/Company/CompanyDetails';
import Contact from './Components/Companies/Company/Contact/Contact';

const Routes = () => {
    return (
        <Switch>
            <Route path="/companies/:id" component={Company} />
            <Route path="/numbers/:number" component={Contact} />
            <Route exact path="/" component={CompaniesList} />
        </Switch>
    )
}

export default Routes